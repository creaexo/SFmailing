from rest_framework import viewsets, status
from rest_framework.response import Response


class DefsAPIVewSetMixin(viewsets.ModelViewSet):

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user.id == instance.user_id:
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        else:
            return Response({'error': 'Нет доступа'})