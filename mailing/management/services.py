
from datetime import timedelta
import requests, json
from django.utils import timezone

from celery_app import app
from management.models import Client, Message, Mailing

@app.task()
def send_message(id_message, phone, text, token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MzAzMDAwNjIsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9zMTFnbnVtIn0.y6PCjdipXQZ9q21odrXvjlJmiD6KSgqunUAPcpqSZ-c'):
    url = f'https://probe.fbrq.cloud/v1/send/{id_message}'
    headers = {
        'accept': 'application/json',
        'Authorization': f'{token}',
        'Content-Type': 'application/json',
    }
    data = {
        "id": id_message,
        "phone": phone,
        "text": f"{text}"
    }
    json_data = json.dumps(data)
    try:
        response = requests.post(url, data=json_data, headers=headers).json()
        return response['message']
    except requests.exceptions.ConnectionError as e:
        return e

@app.task()
def start_mailing1(mailing_id, mailing_user, dt_start, dt_end, text, tag_filter, operator_code_filter, stopped):
    # Разделение фильтров
    tag_filter = tag_filter.split(';')
    operator_code_filter = operator_code_filter.split(';')
    # Конец рассылки для пользователей с последним часовым поясом
    end = dt_end + timedelta(hours=23)
    # Фильтр на подходящие часовые пояса
    time_zone_filter = []
    excluded_users_id = []
    if stopped:
        excluded_users_list = list(Message.objects.filter(mailing_id=mailing_id).values())
        for i in excluded_users_list:
            excluded_users_id.append(i['client_id'])
    for i in range(24):
        if dt_end > (timezone.now()+timedelta(hours=i)):
            time_zone_filter.append(i)
    # Создание списка по фильтрам
    if tag_filter != [''] and operator_code_filter != ['']:
        model = Client.objects.filter(user=mailing_user, tag__in=tag_filter, operator_code__in=operator_code_filter, time_zone__in=time_zone_filter).values().exclude(id__in=excluded_users_id).order_by('time_zone')
    elif tag_filter != [''] and operator_code_filter == ['']:
        model = Client.objects.filter(user=mailing_user, tag__in=tag_filter, time_zone__in=time_zone_filter).values().exclude(id__in=excluded_users_id).order_by('time_zone')
    elif operator_code_filter != [''] and tag_filter == ['']:
        model = Client.objects.filter(user=mailing_user, operator_code__in=operator_code_filter, time_zone__in=time_zone_filter).values().exclude(id__in=excluded_users_id).order_by('time_zone')
    else:
        model = Client.objects.filter(user=mailing_user, time_zone__in=time_zone_filter).values().exclude(id__in=excluded_users_id).order_by('-time_zone')
    model =list(model)
    for i in model:
        if end > timezone.now():
            # Создание оъекта нового сообщения
            new_message = Message.objects.create(status=0, mailing_id=mailing_id, client_id=int(i['id']))
            client_local_time = timezone.now() + timedelta(hours=int(i['time_zone']))
            new_message.save()
            if dt_start > client_local_time:
                send_message.apply_async([new_message.id, i['phone_number'], 'text'],
                                         eta=dt_start+timedelta(hours=int(i['time_zone']))
                                         )
            elif dt_end < client_local_time:
                new_message.status = 2
                new_message.save()
            else:
                response = send_message(id_message=new_message.id, phone=i['phone_number'], text=text)

                if response == 'OK':
                    new_message.status = 1
                    new_message.save()
                else:
                    new_message.status = 3
                    new_message.save()
                    model.append(i) # В случае неудачной отправки, пользователь добавляется в конец очереди, для повторной попытки.

        else:
            break
    try:
        mailing = Mailing.objects.get(id=mailing_id)
        mailing.active = False
        mailing.stopped = True
        mailing.save()
    except Exception as e:
        print(e)
