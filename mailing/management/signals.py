from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Mailing


@receiver(post_save, sender=Mailing)
def func_name(sender, instance, created, **kwargs):
    pass
