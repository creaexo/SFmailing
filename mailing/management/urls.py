from django.urls import path, include, re_path
from .views import *
from rest_framework import routers

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    openapi.Info(
        title='SFmailng API',
        default_version='1.0.0',
        description='API documentation of App'
    ),
    public=True
)



router = routers.DefaultRouter()
router.register('client', ClientViewSet)
router.register('message', MessageViewSet)
router.register('mailing', MailingViewSet)



urlpatterns = [
    path('', index, name='main'), # Главная
    path('add-users/', add_test_users, name='add_users'), # Добавляет пользователей для тестирования
    path('del-users/', del_users, name='del_users'), # Удаляет всех пользователей
    path('api/v1/', include(router.urls)), # API
    path('api/v1/auth-rest/', include('rest_framework.urls')), # Авторизация через rest
    path('api/v1/statistic-messages/', StatisticMessagesAPIView.as_view()), # Статистика по сообщениям
    path('api/v1/statistic-messages/<int:status>/', StatisticMessagesAPIView.as_view()), # Статистика сообщений по статусам
    path('api/v1/messages-in-mailing/<int:mailing_id>/', MessagesInMailingAPIView.as_view()), # Посмотреть сообщения в рассылке
    path('api/v1/auth/', include('djoser.urls')), # Маршрут для регистации пользователей
    re_path(r'^auth/', include('djoser.urls.authtoken')),
    path('api/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='swagger-schema'), # OpenAPI
]
